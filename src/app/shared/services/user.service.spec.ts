import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from './user.service';

describe('UserService', () => {

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule, HttpClientModule],
  }));


  it('UserService should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  it('getUsersList should have been called', () => {
    const service: UserService = TestBed.get(UserService);
    let spy = spyOn(service, 'getUsersList');
    service.getUsersList();
    expect(spy).toHaveBeenCalled();
  });
});
