import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  api: string = 'https://uitest.free.beeceptor.com/usernames';

  constructor(private http: HttpClient) { }


  getUsersList() {
    return this.http.get(this.api);
  }

}
