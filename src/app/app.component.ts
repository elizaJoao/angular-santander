import { Component } from '@angular/core';
import { User } from './models/user.model';
import { UserService } from './shared/services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  user: string = '';
  list: User[] = [];
  storageList: string = localStorage.getItem('users');
  isEditing: boolean = false;
  showEditAll: boolean = true;

  constructor(private userService: UserService) {

  }

  ngOnInit() {
    if(!this.storageList) {
      this.getUsers();
    } else {
      this.list = JSON.parse(this.storageList);
    }
  }


  addNewUser () {
    this.list.unshift({name: this.user, isSelected: false});
    this.user = '';
    this.showEditAll = true;
  }


  getUsers() {
    this.userService.getUsersList().subscribe((data: User[]) => {
      this.list = data;
      this.list.map((item: User) => item.isSelected = false);
      localStorage.setItem('users', JSON.stringify(this.list));
    });
  }

  edit(index: number) {
    this.list[index].isSelected = true;
  }

  remove(index: number) {
    this.list.splice(index, 1);
  }

  save(index: number) {
    this.list[index].isSelected = false;
  }

  deleteAll() {
    this.showEditAll = false;
    this.list = [];
  }

  actionAll(showEditAll: boolean, isSelected: boolean) {
    this.showEditAll = showEditAll;
    this.list.map((item: User)=> item.isSelected = isSelected);
  }

}
