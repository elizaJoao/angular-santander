import { TestBed, async, ComponentFixture } from "@angular/core/testing";
import { AppComponent } from "./app.component";
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';

describe("AppComponent", () => {
  let list = [];
  let users = [];
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, HttpClientModule],
      declarations: [AppComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it("should create the app", () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it("should add new user", () => {
    component.list = [];
    component.addNewUser();

    expect(component.list.length).toBeGreaterThan(0);
  });

  it("should remove a user in list", () => {
    const list =  [
      { name: "Ras Berry", isSelected: false },
      { name: "John Doe", isSelected: false },
      { name: "Gareth Aldridge", isSelected: false },
      { name: "Hallen Pipper", isSelected: false },
      { name: "Joe Allen", isSelected: false },
      { name: "Dolly Johnson", isSelected: false },
    ];

    component.list = list;
    const lengthList = list.length;
    component.remove(2);

    const finalList = component.list;

    expect(finalList.length).toBeLessThan(lengthList);
  });

  it("should remove All users in list", () => {
    const list =  [
      { name: "Ras Berry", isSelected: false },
      { name: "John Doe", isSelected: false },
      { name: "Gareth Aldridge", isSelected: false },
      { name: "Hallen Pipper", isSelected: false },
      { name: "Joe Allen", isSelected: false },
      { name: "Dolly Johnson", isSelected: false },
    ];

    component.list = list;
    component.deleteAll();

    const finalList = component.list;

    expect(finalList.length).toEqual(0);
  });


  it("should save All users in list", () => {
    const list =  [
      { name: "Ras Berry", isSelected: true },
      { name: "John Doe", isSelected: true },
      { name: "Gareth Aldridge", isSelected: true }
    ];

    component.list = list;
    component.actionAll(true, false);

    expect(component.showEditAll).toEqual(true);
    expect(component.list[0].isSelected).toEqual(false);
  });


  it("should edit All users in list", () => {
    const list =  [
      { name: "Ras Berry", isSelected: false },
      { name: "John Doe", isSelected: false },
      { name: "Gareth Aldridge", isSelected: false }
    ];

    component.list = list;
    component.actionAll(false, true);

    expect(component.showEditAll).toEqual(false);
    expect(component.list[0].isSelected).toEqual(true);
  });

  it("should edit a user", () => {
    const list =  [{ name: "Test", isSelected: false }];

    component.list = list;
    component.edit(0);

    expect(component.list[0].isSelected).toBe(true);
  });


  it("should save a user", () => {
    const list =  [{ name: "Test", isSelected: true }];

    component.list = list;
    component.save(0);

    expect(component.list[0].isSelected).toBe(false);
  });
});
